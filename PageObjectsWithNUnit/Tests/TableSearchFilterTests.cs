﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using PageObjectsWithNUnit.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace PageObjectsWithNUnit.Tests
{
    class TableSearchFilterTests
    {
        IWebDriver driver;
        TableSearchFilter tableSearchFilter;


        [SetUp]
        public void Init()
        {
            driver = new ChromeDriver();
            driver.Manage().Cookies.DeleteAllCookies();
            tableSearchFilter = new TableSearchFilter(driver);

        }

        [Test]
        public void ValidateFilterWithInput()
        {
            string message = "John Smith";

            tableSearchFilter.GoToPage();
            tableSearchFilter.EnterFilter(message);

            Assert.That(tableSearchFilter.GetFirstRowName(), Is.EqualTo(message));

        }

        [Test]
        public void ValidateFilterWithEnabledFilter()
        {
            string message = "1";

            tableSearchFilter.GoToPage();
            tableSearchFilter.EnableFilter();
            tableSearchFilter.insertIntoNumberEnabledFilter(message);

            Assert.That(tableSearchFilter.GetFirstRowNumber(), Is.EqualTo(message));
        }

        [TearDown]
        public void Cleanup()
        {
            driver.Quit();
        }

    }
}
