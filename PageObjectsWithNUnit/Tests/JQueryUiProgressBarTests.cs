﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using PageObjectsWithNUnit.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace PageObjectsWithNUnit.Tests
{
    class JQueryUiProgressBarTests
    {
        IWebDriver driver;
        JQueryUiProgressBar jQueryUiProgressBar;


        [SetUp]
        public void Init()
        {
            driver = new ChromeDriver();
            driver.Manage().Cookies.DeleteAllCookies();
            jQueryUiProgressBar = new JQueryUiProgressBar(driver);

        }

        [Test]
        public void ValidateDownloadWithCancel()
        {
            string message = "Cancel Download";
            jQueryUiProgressBar.GoToPage();
            jQueryUiProgressBar.StartDownload();

            Assert.That(jQueryUiProgressBar.CancelButton(), Is.EqualTo(message));
            jQueryUiProgressBar.CancelButtonClick();          
        }

        [Test]
        public void ValidateDownloadWithCompletion()
        {
            string message1 = "Complete!";
            string message2 = "Close";

            jQueryUiProgressBar.GoToPage();
            jQueryUiProgressBar.StartDownload();

            Thread.Sleep(10000);
            Assert.That(jQueryUiProgressBar.CancelButton(), Is.EqualTo(message2));
            Assert.That(jQueryUiProgressBar.checkLabelMessage, Is.EqualTo(message1));
            jQueryUiProgressBar.CancelButtonClick();
        }


        [TearDown]
        public void Cleanup()
        {
            driver.Quit();
        }
    }
}

