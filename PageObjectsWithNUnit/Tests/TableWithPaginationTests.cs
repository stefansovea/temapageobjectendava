﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using PageObjectsWithNUnit.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;

namespace PageObjectsWithNUnit.Tests
{
    class TableWithPaginationTests
    {

        IWebDriver driver;
        TableWithPagination tableWithPagination;


        [SetUp]
        public void Init()
        {
            driver = new ChromeDriver();
            driver.Manage().Cookies.DeleteAllCookies();
            tableWithPagination = new TableWithPagination(driver);

        }

        [Test]
        public void ValidateNavigation()
        {
            tableWithPagination.GoToPage();

            tableWithPagination.GoToSecondPage();
            tableWithPagination.GoToFirstPage();
            tableWithPagination.GoToThirdPage();
            tableWithPagination.GoBack();
            tableWithPagination.GoForward();
            
        }

 
        [TearDown]
        public void Cleanup()
        {
            driver.Quit();
        }
    }
}
