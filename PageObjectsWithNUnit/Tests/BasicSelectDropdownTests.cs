﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using PageObjectsWithNUnit.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace PageObjectsWithNUnit.Tests
{
    class BasicSelectDropdownTests
    {
        [TestFixture]
        public class BasicFormTests
        {
            IWebDriver driver;
            BasicSelectDropdown basicSelectDropdown;


            [SetUp]
            public void Init()
            {
                driver = new ChromeDriver();
                driver.Manage().Cookies.DeleteAllCookies();
                basicSelectDropdown = new BasicSelectDropdown(driver);

            }

            [Test]
            public void ValidateSelectedDay()
            {
                string message = "Sunday";

                basicSelectDropdown.GoToPage();
                basicSelectDropdown.SelectValueFromDropdown(message);

                string actualMessage = "Day selected :- Sunday";
                Assert.That(basicSelectDropdown.GetMessage(), Is.EqualTo(actualMessage));
            }

            [Test]
            public void ValidateMultiSelectList1()
            {
                string message = "First selected option is : Texas";

                basicSelectDropdown.GoToPage();
                basicSelectDropdown.SelectValuesFromMultiSelect("Texas", "Ohio");

                basicSelectDropdown.FirstSelected();

                Assert.That(basicSelectDropdown.GetMessage2(), Is.EqualTo(message));
            }

            [Test]
            public void ValidateMultiSelectList2()
            {
                string message = "Options selected are : Texas Ohio";

                basicSelectDropdown.GoToPage();
                basicSelectDropdown.SelectValuesFromMultiSelect("Texas", "Ohio");

                basicSelectDropdown.GetAllSelected();
                Assert.That(basicSelectDropdown.GetMessage2(), Is.EqualTo(message));
            }


            [TearDown]
            public void Cleanup()
            {
                driver.Quit();
            }
        }
    }
}
