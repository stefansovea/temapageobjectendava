﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace PageObjectsWithNUnit.Pages
{
    class TableWithPagination
    {
        private string url = "https://www.seleniumeasy.com/test/jquery-download-progress-bar-demo.html";

        private IWebDriver driver;

        public TableWithPagination(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }


        [FindsBy(How = How.XPath, Using = "//*[@id='myPager']/li[2]/a")]
        private IWebElement Button1;

        [FindsBy(How = How.XPath, Using = "//*[@id='myPager']/li[3]/a")]
        private IWebElement Button2;

        [FindsBy(How = How.XPath, Using = "//*[@id='myPager']/li[4]/a")]
        private IWebElement Button3;

        [FindsBy(How = How.XPath, Using = "//*[@id='myPager']/li[1]/a")]
        private IWebElement backButton;

        [FindsBy(How = How.XPath, Using = "//*[@id='myPager']/li[5]/a")]
        private IWebElement forwardButton;

        

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void GoToSecondPage()
        {
            Button2.Click();
        }

        public void GoToFirstPage()
        {
            Button1.Click();
        }

        public void GoToThirdPage()
        {
            Button3.Click();
        }

        public void GoBack()
        {
            backButton.Click();
        }

        public void GoForward()
        {
            forwardButton.Click();
        }

       
    }
}
