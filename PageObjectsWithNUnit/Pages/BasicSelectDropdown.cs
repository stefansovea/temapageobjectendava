﻿using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace PageObjectsWithNUnit.Pages
{
    class BasicSelectDropdown
    {
        private string url = "https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html";

        private IWebDriver driver;

        public BasicSelectDropdown(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "select-demo")]
        private IWebElement dropDown;

        [FindsBy(How = How.Id, Using = "multi-select")]
        private IWebElement multiSelect;

        [FindsBy(How = How.Id, Using = "printMe")]
        private IWebElement firstSelectedButton;

        [FindsBy(How = How.Id, Using = "printAll")]
        private IWebElement getAllButton;

        [FindsBy(How = How.ClassName, Using = "selected-value")]
        private IWebElement messageDrop;

        [FindsBy(How = How.ClassName, Using = "getall-selected")]
        private IWebElement messageSelect;

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void SelectValueFromDropdown(string day)
        {
            dropDown.Click();
            SelectElement drop = new SelectElement(dropDown);
            drop.SelectByValue(day);
            
        }

        public void SelectValuesFromMultiSelect(string value1,string value2)
        {
            SelectElement mSel = new SelectElement(multiSelect);
            mSel.SelectByValue(value1);
            
        }

        public void FirstSelected()
        {
            firstSelectedButton.Click();
        }

        public void GetAllSelected()
        {
            getAllButton.Click();
        }

        public string GetMessage()
        {
            return messageDrop.Text;
        }

        public string GetMessage2()
        {
            return messageSelect.Text;
        }

    }
}
