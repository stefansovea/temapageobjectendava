﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace PageObjectsWithNUnit.Pages
{
    class JQueryUiProgressBar
    {
        private string url = "https://www.seleniumeasy.com/test/jquery-download-progress-bar-demo.html";

        private IWebDriver driver;

        public JQueryUiProgressBar(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }


        [FindsBy(How = How.Id, Using = "downloadButton")]
        private IWebElement downloadButton;

        [FindsBy(How = How.XPath, Using = "/html/body/div[3]/div[3]/div/button")]
        private IWebElement closeButton;

        
        [FindsBy(How = How.XPath, Using = "//*[@id='dialog']/div[1]")]
        private IWebElement messageLabel;

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void StartDownload()
        {
            downloadButton.Click();
        }

        public void CancelButtonClick()
        {
            closeButton.Click();
        }

        public string CancelButton()
        {
            return closeButton.Text;
        }

        public string checkLabelMessage()
        {
            return messageLabel.Text;
        }
      
    }
}
