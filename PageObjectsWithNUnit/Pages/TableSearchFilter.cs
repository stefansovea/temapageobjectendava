﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectsWithNUnit.Pages
{
    class TableSearchFilter
    {
        private string url = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";

        private IWebDriver driver;

        public TableSearchFilter(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "task-table-filter")]
         private IWebElement filterInput;

        [FindsBy(How = How.XPath, Using = "//*[@id='task - table']/tbody/tr[1]/td[3]")]
        private IWebElement firstRowName;

        [FindsBy(How = How.XPath, Using = "/html/body/div[2]/div/div[2]/div[2]/div/div/div/button")]
        private IWebElement enableFilterButton;

        [FindsBy(How = How.XPath, Using = "/html/body/div[2]/div/div[2]/div[2]/div/div/div/button")]
        private IWebElement numberInput;


        [FindsBy(How = How.XPath, Using = "/html/body/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[1]")]
        private IWebElement firstRowNumber;
        

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void EnterFilter(string message)
        {
            filterInput.SendKeys(message);
        }

        public string GetFirstRowName()
        {
            return firstRowName.Text;
        }

        public void EnableFilter()
        {
            enableFilterButton.Click();
        }
        public void insertIntoNumberEnabledFilter(string message)
        {
            numberInput.SendKeys(message);
        }
        public string GetFirstRowNumber()
        {
            return firstRowNumber.Text;
        }

    }
}
